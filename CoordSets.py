
# --* coding: utf-8 *--

from numpy.linalg import pinv
from numpy import sqrt, mean, power, dot, absolute
from .PICs import PICList
from .Structure import Molecule

class Redundant(object):
    bt_maxit = 25

    def __init__(self, molecule=None, **kwargs):
        self.mol = molecule
        if not 'pics' in kwargs:
            self.pics = PICList(molecule, pc_set=kwargs.pop('pc_set', None))
            self.pics.determine_PICs()
        else:
            self.pics = kwargs['pics'].get_copy_for_mol(molecule)

    def get_new(self, cart_xyz=None):
        assert cart_xyz is not None
        new_mol = Molecule(from_mol=self.mol, ccoords=cart_xyz)
        return Redundant(new_mol, pics=self.pics)

    @property
    def size(self):
        return len(self.pics)

    def get_categories(self):
        return [p.category for p in self.pics.pic_list]

    def transform_to_cartesian(self, dq=None):
        assert dq is not None
        pic_list = self.pics
        bmat = pic_list.get_Deriv1()
        qint = pic_list.get_Values()
        xcart= pic_list.molecule.cartesian
        qtarget = qint + dq

        converged, last_rms, curr_rms = False, None, None
        btpinvt = pinv(bmat.T).T
        pic_list.set_Phases()

        dx_guess = dot(btpinvt, dq)
        xguess_first = xcart + dx_guess
        qguess_first = pic_list.get_Values(tmp_carts=xguess_first)
        xtarget_guess = xguess_first
        qtarget_guess = qguess_first
        dq_guess = qtarget - qtarget_guess
        for it in range(self.bt_maxit):
            last_rms = sqrt(mean(power(dx_guess, 2)))
            dx_guess = dot(btpinvt, dq_guess)
            curr_rms = sqrt(mean(power(dx_guess, 2)))
            xtarget_guess += dx_guess
            qtarget_guess = pic_list.get_Values(tmp_carts=xtarget_guess)
            dq_guess = qtarget - qtarget_guess
            if curr_rms < 1.e-6 or absolute(curr_rms - last_rms) < 1.e-12:
                converged = True
                xfinal = xtarget_guess
                qfinal = qtarget_guess
                break
        else:
            xfinal = xguess_first
            qfinal = qguess_first
        return (converged, xfinal, qfinal)

    def get_values(self):
        return self.pics.get_Values()

    def get_gradient(self, cart_grad=None):
        assert cart_grad is not None
        bmat = self.pics.get_Deriv1()
        bpinv, btpinv = pinv(bmat), pinv(bmat.T)
        proj = dot(bmat, bpinv)
        grad_int = dot(btpinv, cart_grad)
        return dot(proj, grad_int)

    def transform_hessian(self, int_grad=None, hess=None):
        assert hess is not None
        bmat = self.pics.get_Deriv1()
        bpinv, btpinv = pinv(bmat), pinv(bmat.T)
        hess_int = hess.get_internal()
        proj = dot(bmat, bpinv)
        return dot(proj, dot(hess_int, proj))
