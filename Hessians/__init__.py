
# --* coding: utf-8 *--

import warnings
import numpy as np
from numpy import outer, inner, dot

class Hessian(object):

    def __init__(self, start_coords=None):
        """ Saves cartesian coordinates with which the class was instantiated.
        An instance thus corresponds to just one geometry."""
        assert start_coords is not None
        self.coords = start_coords
        self.internal = None
        self.cartesian = None
        self.hessclass = None

    def get_cartesian(self):
        return NotImplemented

    def get_internal(self):
        return NotImplemented

    def get_updated_hessian_using_bfgs(self, curr_coords=None, curr_grad=None, old_grad=None, **kwargs):
        assert None not in (curr_coords, curr_grad, old_grad)
        use_internal = kwargs.pop('use_internal', True)
        if use_internal:
            # TODO: convert to internal if only cartesian available
            hess_old = self.get_internal()
            dx = curr_coords.get_values() - self.coords.get_values()
        else:
            # TODO: convert to cartesian if only internal available
            hess_old = self.get_cartesian()
            dx = curr_coords - self.coords.mol.cartesian
        dg = curr_grad - old_grad
        ggt = outer(dg, dg.T)  # transposing actually shouldn't make a difference for vectors
        gtx = inner(dg.T, dx)
        GxxtG = dot(hess_old, dot(outer(dx, dx.T), hess_old))
        xtGx = dot(dx, dot(hess_old, dx))
        correction = ggt / gtx - GxxtG / xtGx
        if np.amax(np.absolute(correction)) > 5.0:
            warnings.warn('Unreasonable correction size! gtx: {:8.2e} xtGx: {:8.2e} Not updating Hessian!'.format(gtx, xtGx))
            correction = 0.0
        new_hess = self.hessclass(curr_coords)
        if use_internal:
            new_hess.internal = hess_old + correction
        else:
            new_hess.cartesian = hess_old + correction
        return new_hess


class Diagonal(Hessian):

    def __init__(self, start_coords=None):
        super(Diagonal, self).__init__(start_coords)
        self.hessclass = Diagonal

    def get_cartesian(self):
        if self.cartesian is None:
            if self.internal is None:
                self.cartesian = np.identity(self.coords.mol.cartesian.shape[0])
            else:
                self.cartesian = self.coords.inthess_to_cart(self.internal)
        return self.cartesian

    def get_internal(self):
        if self.internal is None:
            if self.cartesian is None:
                result = np.zeros((self.coords.size, self.coords.size))
                cats = self.coords.get_categories()
                for c in range(len(cats)):
                    cat = cats[c]
                    if cat == 0: # stretch type
                        result[c, c] = 0.5
                    elif cat == 1: # bend type
                        result[c, c] = 0.2
                    elif cat == 2: # twist type
                        result[c, c] = 0.1
                self.internal = result
            else:
                self.internal = self.coords.carthess_to_int(self.cartesian) # should be (BT)+(H-K)B+
        return self.internal