
# --* coding: utf-8 *--

import Molpro

class OrcaRun(Molpro.MolproRun):
    def __init__(self, *args, **kwargs):
        super(OrcaRun, self).__init__(*args, **kwargs)

    def set_defaults(self):
        self.program        = 'orca'
        self.options        = ''
        self.en_match       = 'FINAL SINGLE POINT ENERGY'
        self.grad_skip      = 2
        self.grad_match     = 'CARTESIAN GRADIENT'
        self.grad_stop      = 'Norm of the cartesian gradient'
        self.hess_match     = ''
        self.xyzininp       = '{xyz}'
        self.grad_pos       = (-3,-2,-1)
        self.write_stdout   = True
        self.errors_pos     = ('NOT CONVERGED',)
        self.errors_neg     = []

    def get_start_Mol(self, **kwargs):
        if 'opath' in kwargs:
            opath   = kwargs['opath']
        else:
            opath   = os.path.join(self.work_dir, self.file_basename) + '.out'

        # don't need to succeed for the input structure.
        structure_str = ''
        start = False
        with open(opath, 'r') as ofile:
            for line in ofile:
                lsplit = line.lstrip()
                if len(lsplit) > 0:
                    if 'CARTESIAN COORDINATES (ANGSTROEM)' in lsplit:
                        if start is False: start = True
                        next(ofile) # skip '-----------'
                    elif start is True:
                        structure_str += lsplit
                else:
                    if start is True:
                        break

        mol = Molecule()
        mol.load_from_String(sstring = structure_str, stype = 'XYZ')
        return mol

    def get_next_Hessian(self, match_str = None, **kwargs):
        raise NotImplementedError('Exactly what it says.')
