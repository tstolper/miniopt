
# --* coding: utf-8 *--

from numpy import dot, argmin, vstack, append, column_stack, sqrt, mean, power, max, absolute
from numpy.linalg import eigh, norm

from .Structure import Molecule


class Convergence(object):
    # values = { 'energy': None, 'max_grad': None, 'grad_norm': None, 'rms_geom': None }
    paths = []

    @classmethod
    def is_converged(cls, **kwargs):
        en = kwargs.pop('energy', None)
        grad_cart = kwargs.pop('gradient', None)
        step_cart = kwargs.pop('step', None)
        assert en is not None
        assert grad_cart is not None
        assert step_cart is not None
        cvalues = {'energy': absolute(en), 'max_grad': max(absolute(grad_cart)),
                   'grad_norm': norm(grad_cart), 'rms_geom': sqrt(mean(power(step_cart, 2)))}
        converged = []
        for p in cls.paths:
            pathconv = True
            for k, v in p.items():
                pathconv = pathconv and cvalues[k] < v
            converged.append(pathconv)
            if pathconv:
                print('Converged in {0}.'.format('and'.join(p.keys())))
        return True in converged

class MolproConv(Convergence):
    paths = [ {'energy': 1.0e-6, 'max_grad': 3.0e-4},
              {'energy': 1.0e-6, 'rms_geom': 3.0e-4}
            ]

def get_RFO_Step(coords=None, gradient=None, hessian=None):
    assert None not in (coords, gradient, hessian)
    lastrow = append(gradient, 0.0)
    aughess = vstack((column_stack((hessian, gradient)), lastrow))
    eigenvals, eigenvecs = eigh(aughess)
    # scale eigenvector
    lowestev = argmin(eigenvals)
    if eigenvals[lowestev] > 1.0E-4:
        raise ValueError('ERROR: I don\'t want to go up the PES!')
    else:
        stepvec = eigenvecs[:, lowestev]
        if stepvec[-1] == 0.0:
            scaledstepvec = stepvec[:-1]
            warnings.warn('Step should be divided by 0! No guarantee for the step size!')
        else:
            scaledstepvec = stepvec[:-1] / stepvec[-1]
    if norm(scaledstepvec) > 0.3:
        scaledstepvec *= 0.3/norm(scaledstepvec)
    return scaledstepvec


class BFGS(object):

    def __init__(self, step_func=None, coordset=None, hessian=None, scf=None, convcrit=None):
        self.hessians = [hessian]
        self.do_step = step_func
        self.coords = [coordset]
        self.scf = scf
        self.conv = convcrit
        self.optstuff = [self._get_last()]

    def _get_last(self):
        energy, gradient = self.scf.get_EnGrad(self.coords[-1].mol)
        qint = self.coords[-1].get_values()
        gint = self.coords[-1].get_gradient(gradient)
        hint = self.coords[-1].transform_hessian(gint, self.hessians[-1])
        return self.coords[-1].mol.cartesian, energy, gradient, qint, gint, hint

    def set_new_Hessian(self, hess_class=None):
        assert hess_class is not None
        xcart, energy, gradient, qint, gint, hint = self.optstuff[-1]
        hint_new = self.coords[-1].get_Hessian(gint, hess_class)
        self.hessians[-1] = hess_class
        self.optstuff[-1] = xcart, energy, gradient, qint, gint, hint_new

    def _make_new_step(self, new_cartesians=None, prev_step=-1):
        assert new_cartesians is not None
        xcart, energy, gradient, qint, gint, hint = self.optstuff[prev_step]
        new_coords = self.coords[prev_step].get_new(new_cartesians)
        # next geometry
        energy_new, gradient_new = self.scf.get_EnGrad(new_coords.mol)
        qint_new = new_coords.get_values()
        gint_new = new_coords.get_gradient(gradient_new)
        hint_new_class = self.hessians[prev_step].get_updated_hessian_using_bfgs(new_coords, gint_new, gint)
        hint_new = new_coords.transform_hessian(gint_new, hint_new_class)
        return new_coords, hint_new_class, new_cartesians, energy_new, gradient_new, qint_new, gint_new, hint_new

    def change_last_step(self, new_dx=None):
        assert new_dx is not None
        prev_xcart = self.optstuff[-2][0]
        new_coords, hint_new_class, xcart_new, energy_new, gradient_new, qint_new, gint_new, hint_new = self._make_new_step(new_cartesians=prev_xcart+new_dx, prev_step=-2)
        self.coords[-1] = new_coords
        self.hessians[-1] = hint_new_class
        self.optstuff[-1] = xcart_new, energy_new, gradient_new, qint_new, gint_new, hint_new
        return energy_new, gradient_new, xcart_new, qint_new

    def __iter__(self):
        return self

    def __next__(self):
        xcart, energy, gradient, qint, gint, hint = self.optstuff[-1]
        if len(self.optstuff) > 1:
            xcart_old, energy_old = self.optstuff[-2][:2]
            if self.conv.is_converged(energy=energy-energy_old, gradient=gradient, step=xcart-xcart_old):
                raise StopIteration
        # get new step
        qstep = self.do_step(qint, gint, hint)
        converged, xcart_new, __ = self.coords[-1].transform_to_cartesian(qstep)
        new_coords, hint_new_class, xcart_new, energy_new, gradient_new, qint_new, gint_new, hint_new = self._make_new_step(new_cartesians=xcart_new)
        self.coords.append(new_coords)
        self.hessians.append(hint_new_class)
        self.optstuff.append((xcart_new, energy_new, gradient_new, qint_new, gint_new, hint_new))
        return energy_new, gradient_new, xcart_new, qint_new
