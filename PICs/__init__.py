# --* coding: utf-8 *--

import scipy as np
import scipy.linalg as linalg


class PrimCoord(object):

    def __init__(self, atoms, *args, **kwargs):
        raise NotImplementedError('This is just a template')

    def __eq__(self, other):
        return self.type == other.type and False not in [a in other.atoms for a in self.atoms]

    def get_Value(self, ccoords, *args, **kwargs):
        raise NotImplementedError('This is just a template')

    def get_Deriv1(self, ccoords, *args, **kwargs):
        raise NotImplementedError('This is just a template')

    def get_Deriv2(self, ccoords, *args, **kwargs):
        raise NotImplementedError('This is just a template')


class PICList(object):
    def __init__(self, mol=None, pc_set=None):
        assert mol is not None
        if pc_set is None:
            import miniopt.PICs.default
            module = miniopt.PICs.default
        else:
            module = pc_set
        self.pic_module = module
        self.pic_list   = []
        self.molecule   = mol
        self._values = None
        self._deriv1 = None
        self._deriv2 = None

    def __eq__(self, other):
        if other is None:
            return False
        elif isinstance(other, PICList):
            return self is other or False not in [ pic in other.pic_list for pic in self.pic_list ]
        else:
            raise ValueError('Comparison not defined!')

    def __sub__(self, other):
        assert self == other
        #return [ spic - opic for spic, opic in zip(self.pic_list, other.pic_list) ]
        return NotImplemented

    def __len__(self):
        return len(self.pic_list)

    def __getitem__(self, item):
        return self.pic_list[item]

    def __iter__(self):
        return iter(self.pic_list)

    def determine_PICs(self):
        dists = get_Distances(self.molecule.cartesian)
        for pic in self.pic_module.get_PICs(self.molecule.cartesian, dists, self.molecule.symbols):
            if pic not in self.pic_list:
                self.pic_list.append(pic)
            else:
                raise ValueError('PIC "'+str(pic)+'" already in List!')
        self.pic_list.sort(key=lambda pic: (len(pic.atoms),) + pic.atoms[:2])

    def get_copy_for_mol(self, new_mol=None):
        assert new_mol is not None
        newlist = PICList(mol=new_mol, pc_set=self.pic_module)
        newlist.pic_list = self.pic_list
        return newlist

    def get_PIC_Tuples(self):
        return [pic.atoms for pic in self.pic_list]

    def set_Phases(self):
        ccoords = self.molecule.cartesian
        for pic in self.pic_list:
            if pic.type == 'twist':
                pic.set_phase(ccoords)

    def get_Values(self, tmp_carts=None):
        if tmp_carts is None:
            if self._values is None or len(self.pic_list) != len(self._values):
                ccoords = self.molecule.cartesian
                self._values = np.array([pic.get_Value(ccoords) for pic in self.pic_list])
            return self._values
        else:
            return np.array([pic.get_Value(tmp_carts) for pic in self.pic_list])

    def get_NumDeriv1(self, **kwargs):
        ccoords = kwargs.pop('tmp_carts', self.molecule.cartesian)
        cartdiff= kwargs.pop('diff', 0.002)
        num_pics = np.zeros((len(self.pic_list), len(ccoords)))
        for pic in range(len(self.pic_list)):
            picobj = self.pic_list[pic]
            atoms = picobj.atoms
            if picobj.type == 'twist':
                picobj.set_phase(ccoords)
            for ix in range(len(atoms)*3):
                zero = np.zeros(ccoords.shape)
                zero[atoms[ix//3]*3 + ix%3] = cartdiff
                pos = picobj.get_Value(ccoords + zero)
                neg = picobj.get_Value(ccoords - zero)
                num_pics[pic, atoms[ix//3]*3 + ix%3] = (pos - neg)/(2.0*cartdiff)
        return num_pics

    def get_Deriv1(self, tmp_carts=None):
        if tmp_carts is None:
            if self._deriv1 is None or self._deriv1.shape[0] != len(self._deriv1):
                ccoords = self.molecule.cartesian
                self._deriv1 = np.vstack([ pic.get_Deriv1(ccoords) for pic in self.pic_list ])
            return self._deriv1
        else:
            return np.vstack([ pic.get_Deriv1(tmp_carts) for pic in self.pic_list ])

    def get_Deriv2(self, tmp_carts=None):
        raise NotImplementedError('Not done yet.')

    def __str__(self):
        return '\n'.join(str(pic) for pic in self.pic_list)


atom_distances = None
opt_init_hooks = None
opt_itstart_hook = None
opt_itend_hook = None


def get_Distances(coords=None):
    assert coords is not None
    assert len(coords.shape) == 1, 'coords.shape = {0}'.format(coords.shape)

    natoms = len(coords)//3
    conns = np.zeros((natoms, natoms))
    for i in range(natoms):
        for j in range(i + 1, natoms):
            conns[i, j] = linalg.norm(coords[j * 3:j * 3 + 3] - coords[i * 3:i * 3 + 3])
            conns[j, i] = conns[i, j]
    return conns

