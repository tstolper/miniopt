
# --* coding: utf-8 *--

import warnings
import scipy as np
import scipy.linalg as linalg

from miniopt.utilities import Elements, Bohr2Ang, _calc_Angle_of_Vects, _calcAngle, _norm_is_ok
from miniopt.PICs import PrimCoord
from .PIC_settings import *


def get_PICs_by_atomcomb(combins=None, *args, **kwargs):
    for acomb in combins:
        for pic in pics_by_atoms[len(acomb)]:
            for p in pic.get_PICs_for_atoms(acomb, *args, **kwargs):
                yield p


def is_std_bond(atoms=None, ccoords=None, symbols=None, dists=None):
    if len(atoms) != 2:
        raise ValueError('Wrong number of atoms!')
    else:
        atom_a, atom_b  = atoms
        cov_sum         = getattr(Elements, symbols[atom_a]).cov_radius + getattr(Elements, symbols[atom_b]).cov_radius
        return (dists[atom_a, atom_b] <= pic_constants['covbond_fact']*cov_sum)


def is_h_bond(atoms=None, ccoords=None, symbols=None, dists=None):
    """ Determines H-bond (thus, an H connected to a donor atom binding to the heteroatom)
        Returns a list of tuples with the atom indices forming the H-bond in the order (donor-atom, h-atom, acceptor-atom).
        Thus if there is no H-bond in which atom1 and atom2 are the hydrogen and acceptor atom, the result will be
        an empty list and evaluate to False. 
    """
    if len(atoms) != 2:
        raise ValueError('Wrong number of atoms!')
    else:
        atom_a, atom_b = atoms
        if symbols[atom_a] == 'H' and symbols[atom_b] in acceptor_elements:
            h_atom, acc_atom = atom_a, atom_b
        elif symbols[atom_b] == 'H' and symbols[atom_a] in acceptor_elements:
            h_atom, acc_atom = atom_b, atom_a
        else:
            h_atom, acc_atom = None, None
            return []
        # find heteroatom bound to H
        don_atoms = [ atom for atom in range(len(symbols)) if atom not in (h_atom, acc_atom) and symbols[atom] in donor_elements and is_std_bond((h_atom, atom), ccoords, symbols, dists) ]
        # check if D-H----A combinations fulfill our definition of H-bonds
        real_don_atoms = [] # donor atoms corresponding to real H-bonds
        for atom in don_atoms:
            cov_sum = getattr(Elements, 'H').cov_radius + getattr(Elements, symbols[atom]).cov_radius
            vdw_sum = getattr(Elements, 'H').vdw_radius + getattr(Elements, symbols[atom]).vdw_radius
            hb_angle= np.dot(ccoords[atom*3:atom*3+3]-ccoords[h_atom*3:h_atom*3+3], ccoords[acc_atom*3:acc_atom*3+3]-ccoords[h_atom*3:h_atom*3+3])
            if dists[h_atom, atom] > cov_sum and dists[h_atom, atom] < vdw_sum and hb_angle < 0.0:
                real_don_atoms.append(atom)
        return [(atom, h_atom, acc_atom) for atom in real_don_atoms]

def is_extra_bond(atoms=None, ccoords=None, symbols=None, dists=None):
    if len(atoms) != 2:
        raise ValueError('Wrong!')
    else:
        atom_a, atom_b  = atoms
        cov_sum         = getattr(Elements, symbols[atom_a]).cov_radius + getattr(Elements, symbols[atom_b]).cov_radius
        return dists[atom_a, atom_b] < pic_constants['auxbond_fact']*cov_sum

def get_bonds(ccoords=None, symbols=None, dists=None):
    conn = np.zeros(dists.shape, dtype=int)
    for i in range(len(symbols)):
        for j in range(i+1, len(symbols)):
            tval = 0
            if is_std_bond((i, j), ccoords, symbols, dists):
                tval += bond_types['standard']
            if is_h_bond((i,j), ccoords, symbols, dists):
                tval += bond_types['hbond']
            if is_extra_bond((i,j), ccoords, symbols, dists):
                tval += bond_types['extra']
            conn[i,j] = tval
            conn[j,i] = tval
    return conn


class Stretch(PrimCoord):
    category = 0
    max_atoms = 2

    @classmethod
    def get_PICs_for_atoms(cls, combin=None, dists=None, bonds=None, ccoords=None, symbols=None, doextra=False):
        """
        combin: tuple of atom ids of contributing to the coordinate
        dists:  2d-array of atomic distances
        ccoords: flat array of cartesian coordinates
        symbols: atomic symbols
        doextra: flag whether extra redundant coordinates should be created
        """
        if len(combin) != cls.max_atoms:
            raise ValueError('Need {0} atoms for a stretch. Got: '.format(cls.max_atoms) + repr(combin))
        else:
            returned_str    = []
            btype           = bonds[combin[0], combin[1]]
            # "normal" bond
            if btype & bond_types['standard']:
                # this is a covalent bond
                returned_str.append(cls(combin[0], combin[1], hbond=False, extra=False))
            elif btype & bond_types['hbond']: # find acceptor
                returned_str.append(cls(combin[0], combin[1], hbond=True, extra=False))
            elif doextra and btype & bond_types['extra']:
                returned_str.append(cls(combin[0], combin[1], hbond=False, extra=True))
            return returned_str

    def __init__(self, atom1, atom2, hbond=False, extra=False):
        self.type = 'stretch'
        self.is_hbond = hbond
        self.is_extra = extra
        if atom1 < atom2:
            self.atoms = (atom1, atom2)
        else:
            self.atoms = (atom2, atom1)

    def __eq__(self, other):
        return (self.atoms == other.atoms)

    def __str__(self):
        return 'Bond ({0:3},{1:3})     hbond={2}, extra={3}'.format(self.atoms[0], self.atoms[1], self.is_hbond, self.is_extra)

    def get_Value(self, ccoords=None):
        assert ccoords is not None
        a1      = self.atoms[0]
        a2      = self.atoms[1]
        dist    = linalg.norm(ccoords[a2*3:a2*3+3]-ccoords[a1*3:a1*3+3])
        return dist

    def get_Deriv1(self, ccoords=None):
        deriv  = np.zeros(ccoords.shape)
        a1, a2 = self.atoms[0], self.atoms[1]
        vec    = ccoords[a2*3:a2*3+3]-ccoords[a1*3:a1*3+3]
        dist   = linalg.norm(vec)
        deriv[a1*3:a1*3+3] = -vec/dist
        deriv[a2*3:a2*3+3] =  vec/dist
        return deriv

    def get_Deriv2(self, ccoords=None):
        deriv2 = np.zeros((ccoords.shape[0], ccoords.shape[0]))
        a1, a2 = self.atoms[0], self.atoms[1]
        vec    = ccoords[a2*3:a2*3+3]-ccoords[a1*3:a1*3+3]
        dist   = linalg.norm(vec)
        sign_mat = -1.0*np.ones((3,3)) + 2.0*np.identity(3) # 1 diagonal, -1 off-diagonal
        # diagonal contributions
        deriv2[a1*3, a1*3]     = -vec[0]**2/dist**3 + 1.0/dist
        deriv2[a1*3+1, a1*3+1] = -vec[1]**2/dist**3 + 1.0/dist
        deriv2[a1*3+2, a1*3+2] = -vec[2]**2/dist**3 + 1.0/dist
        # off-diagonal contributions
        deriv2[a1*3, a1*3+1]   = vec[0]*vec[1]/dist**3
        deriv2[a1*3, a1*3+2]   = vec[0]*vec[2]/dist**3
        deriv2[a1*3+1, a1*3]   = deriv2[a1*3, a1*3+1]
        deriv2[a1*3+1, a1*3+2] = vec[1]*vec[2]/dist**3
        deriv2[a1*3+2, a1*3]   = deriv2[a1*3, a1*3+2]
        deriv2[a1*3+2, a1*3+1] = deriv2[a1*3+1, a1*3+2]
        # equivalences
        deriv2[a1*3, a2*3]     = -deriv2[a1*3, a1*3]
        deriv2[a1*3, a2*3+1]   = -deriv2[a1*3, a1*3+1]
        deriv2[a1*3, a2*3+2]   = -deriv2[a1*3, a1*3+2]
        deriv2[a1*3+1, a2*3]   = deriv2[a1*3, a2*3+1]
        deriv2[a1*3+1, a2*3+1] = -deriv2[a1*3+1, a1*3+1]
        deriv2[a1*3+1, a2*3+2] = -deriv2[a1*3+1, a1*3+1]
        deriv2[a1*3+2, a2*3]   = deriv2[a1*3, a2*3+2]
        deriv2[a1*3+2, a2*3+1] = deriv2[a1*3+1, a2*3+2]
        deriv2[a1*3+2, a2*3+2] = -deriv2[a1*3+2, a1*3+2]

        deriv2[a2*3:a2*3+3, a2*3:a2*3+3] = np.multiply(deriv2[a1*3:a1*3+3, a1*3:a1*3+3], sign_mat)
        deriv2[a2*3:a2*3+3, a1*3:a1*3+3] = np.multiply(deriv2[a1*3:a1*3+3, a2*3:a2*3+3], sign_mat)
        return deriv2


class Bend(PrimCoord):
    category = 1
    max_atoms = 3

    @classmethod
    def get_PICs_for_atoms(cls, combin=None, dists=None, bonds=None, ccoords=None, symbols=None, doextra=False):
        """ Determines all applicable bends for any of three atoms, i.e. all combinations that
            make up a proper bend.
        """
        if len(combin) != cls.max_atoms:
            raise ValueError('Too many atoms for bend! len(combin)={}'.format(len(combin)))
        else:
            returned_bends = []
            angle_triples = ( combin, (combin[0], combin[2], combin[1]), (combin[1], combin[0], combin[2]) )
            use_bonds = bond_types['standard']+bond_types['hbond']
            for l,m,r in angle_triples:
                if bonds[l,m] & use_bonds:
                    if bonds[m,r] & use_bonds:
                        angle = _calcAngle(ccoords, l, m, r)
                        if angle <= np.pi-0.2:
                            returned_bends.append(cls(l, m, r))
                        else:
                            # add linear bend
                            returned_bends.append(LinBendCart(l, m, r))
                            returned_bends.append(LinBendCart(l, m, r, vers=1))
                            warnings.warn('Found linear bend!' + str(returned_bends[-1]))
            # if len(returned_bends) > 0:
            #     print "Found "+str(len(returned_bends))+" bends for "+repr(combin)
            return returned_bends

    def __init__(self, atoml=None, atomm=None, atomr=None):
        self.type = 'bend'
        self.is_linear = False
        if atoml > atomr:
            self.atoms = (atomr, atomm, atoml)
        else:
            self.atoms = (atoml, atomm, atomr)

    def __eq__(self, other):
        iseq = (self.type == other.type and self.atoms == other.atoms)
        if iseq and isinstance(self, LinBendCart):
            iseq = (self.linvers == other.linvers)
        return iseq

    def __str__(self):
        return 'Angle ({0:3},{1:3},{2:3})  linear={3}'.format(self.atoms[0], self.atoms[1], self.atoms[2], self.is_linear)

    def get_Value(self, ccoords=None):
        assert ccoords is not None
        a, b, c = self.atoms
        uvec = ccoords[a * 3:a * 3 + 3] - ccoords[b * 3:b * 3 + 3]
        vvec = ccoords[c * 3:c * 3 + 3] - ccoords[b * 3:b * 3 + 3]
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        uvec, vvec = uvec / ulen, vvec / vlen
        return _calc_Angle_of_Vects(uvec, vvec)[0]

    def get_Deriv1(self, ccoords=None):
        assert ccoords is not None
        retval = np.zeros(ccoords.shape)
        a, b, c = self.atoms
        uvec = ccoords[a * 3:a * 3 + 3] - ccoords[b * 3:b * 3 + 3]
        vvec = ccoords[c * 3:c * 3 + 3] - ccoords[b * 3:b * 3 + 3]
        ulen, vlen = linalg.norm(uvec), linalg.norm(vvec)
        f1 = -1.0 / np.sqrt(1.0 - np.dot(uvec / ulen, vvec / vlen) ** 2)
        dpda = (vvec * vlen * ulen - np.dot(uvec, vvec) * uvec * vlen / ulen) / (ulen * vlen) ** 2
        dpdb = (-(vvec + uvec) * vlen * ulen - np.dot(uvec, vvec) * (-uvec * vlen / ulen - vvec * ulen / vlen)) / (ulen * vlen) ** 2
        dpdc = (uvec * vlen * ulen - np.dot(uvec, vvec) * vvec * ulen / vlen) / (ulen * vlen) ** 2
        retval[a*3:a*3+3], retval[b*3:b*3+3], retval[c*3:c*3+3] = f1 * dpda, f1 * dpdb, f1 * dpdc
        return retval

    def get_Deriv2(self, ccoords=None):
        return NotImplemented


class LinBendCart(Bend):

    def __init__(self, *args, **kwargs):
        super(LinBendCart, self).__init__(*args)
        self.type = 'linbendcart'
        self.is_linear = True
        self.linvers = kwargs.pop('vers', 0)
        self.cids = None

    def _get_Cart_ID(self, ccoords=None):
        from operator import itemgetter
        assert ccoords is not None
        # choose the cartesian directions of b most orthogonal
        # to the line connecting atom a and c.
        a, b, c = self.atoms
        uvec = ccoords[a * 3:a * 3 + 3] - ccoords[b * 3:b * 3 + 3]
        vvec = ccoords[c * 3:c * 3 + 3] - ccoords[b * 3:b * 3 + 3]
        ulen = linalg.norm(uvec)
        vlen = linalg.norm(vvec)
        acvec = uvec - vvec
        aclen = linalg.norm(acvec)
        maxorth = sorted(enumerate(np.absolute(acvec / aclen)), key=itemgetter(1))
        return [i for i, v in maxorth[:2]]

    def get_Value(self, ccoords=None):
        assert ccoords is not None
        retval = None
        b = self.atoms[1]
        ccids = self._get_Cart_ID(ccoords)
        if self.cids is None:
            self.cids = ccids
        elif self.cids[0] not in ccids or self.cids[1] not in ccids:
            warnings.warn('Preferred cartesian changes! That is inconsistant! ccids=' + repr(ccids) + ', cids=' + repr(
                    self.cids))
        if self.linvers < 1:
            retval = ccoords[b * 3 + self.cids[0]]
        else:
            retval = ccoords[b * 3 + self.cids[1]]
        return retval

    def get_Deriv1(self, ccoords=None):
        self.get_Value(ccoords)
        retval = np.zeros(ccoords.shape)
        if self.linvers < 1:
            retval[self.atoms[1]*3 + self.cids[0]] = 1.0
        else:
            retval[self.atoms[1]*3 + self.cids[1]] = 1.0
        return retval

    def get_Deriv2(self, ccoords=None):
        return NotImplemented


class Twist(PrimCoord):
    category = 2
    max_atoms = 4
    ang_max = 175.0*np.pi/180.0
    ang_min = 5.0*np.pi/180.0
    phase = 0

    @classmethod
    def _get_Set_of_nonlin_BP(cls, flatcoords=None, catom=None, oatom=None, bonds=None):
        ret_bps = []
        bonding_atoms = np.nonzero(bonds[catom, :])
        for atom in bonding_atoms:
            if atom != oatom:
                angle = _calcAngle(flatcoords, atom, catom, oatom)
                if angle < cls.ang_max:
                    ret_bps.append(atom)
        return ret_bps

    @classmethod
    def get_PICs_for_atoms(cls, combin=None, dists=None, bonds=None, ccoords=None, symbols=None, doextra=False):
        if len(combin) != cls.max_atoms:
            raise ValueError('Too many atoms for twist! len(combin)={}'.format(len(combin)))
        else:
            returned_twists = []
            # just determine any possible torsion that doesn't have a linear angle
            use_bonds = bond_types['standard']+bond_types['hbond']
            c = ( combin,
                 (combin[0], combin[1], combin[3], combin[2]),
                 (combin[0], combin[2], combin[1], combin[3]),
                 (combin[0], combin[2], combin[3], combin[1]),
                 (combin[0], combin[3], combin[1], combin[2]),
                 (combin[0], combin[3], combin[2], combin[1]),
                 (combin[1], combin[0], combin[2], combin[3]),
                 (combin[1], combin[0], combin[3], combin[2]),
                 (combin[1], combin[2], combin[0], combin[3]),
                 (combin[1], combin[3], combin[0], combin[2]),
                 (combin[2], combin[0], combin[1], combin[3]),
                 (combin[2], combin[1], combin[0], combin[3])
                )
            for i,j,k,l in c:
                if bonds[i,j] & use_bonds:
                    if bonds[j,k] & use_bonds:
                        if bonds[k,l] & use_bonds:
                            ang1 = _calcAngle(ccoords, i, j, k)
                            nbonds = np.count_nonzero(bonds[:,j])
                            if ang1 < cls.ang_min:
                                # linear part, but wrong order. should be handled.
                                print('ARG?')
                                pass
                            elif ang1 > cls.ang_max and nbonds == 2:
                                print('OH DEAR!')
                                #print 'linear part!'
                                ang2 = _calcAngle(ccoords, j, k, l)
                                if ang2 > cls.ang_min and ang2 < cls.ang_max: # make sure only one direction is linear
                                    # linear part: add all proper twists formed by atoms bound to i and k (and so on)
                                    # search chain until atom does not have linear parts anymore
                                    l_atoms = []
                                    atom2 = i
                                    while len(l_atoms) == 0:
                                        atom2 = [ __ for __ in np.nonzero(bonds[atom2, :]) if __ != k ][0]
                                        l_atoms = cls._get_Set_of_nonlin_BP(ccoords, atom2, k, bonds)
                                    # and that should be it! Wasn't that difficult now, was it?
                                    # But it might also be not quite good ...
                                    if len(l_atoms) == 0:
                                        raise ValueError('Did not find a dihedral for atoms {0},{1},{2},{3}.'.format(i,j,k,l))
                                    else:
                                        for atom1 in l_atoms:
                                            returned_twists.append(cls(atom1, atom2, k, l))
                            else: # we got a normal angle, so go on with the sequence
                                ang2 = _calcAngle(ccoords, j, k, l)
                                if ang2 < cls.ang_max and ang2 > cls.ang_min: # decent twist
                                    twist = cls(i, j, k, l)
                                    if twist not in returned_twists:
                                        returned_twists.append(twist)
                                else:
                                    # ignore, since the linear part will show up as ijk
                                    print('EWWW')
                                    pass
            # if len(returned_twists) > 0:
            #     print "Found "+str(len(returned_twists))+" twists for "+repr(combin)
            return returned_twists

    def __init__(self, atomi=None, atomj=None, atomk=None, atoml=None):
        self.type = 'twist'
        if atomi > atoml:
            self.atoms = (atoml, atomk, atomj, atomi)
        else:
            self.atoms = (atomi, atomj, atomk, atoml)

    def __eq__(self, other):
        return self.type == other.type and self.atoms == other.atoms

    def __str__(self):
        return 'Twist ({0:3},{1:3},{2:3},{3:3})'.format(*self.atoms)

    def set_phase(self, ccoords=None, nophase=False):
        assert ccoords is not None
        val = self.get_Value(ccoords, nophase)
        if val > np.pi/2.0:
            self.phase = 1
        elif val < -np.pi/2.0:
            self.phase = -1
        else:
            self.phase = 0

    def get_Value(self, ccoords=None, nophase=False):
        assert ccoords is not None
        matom, oatom, patom, natom = self.atoms
        uvec = ccoords[oatom * 3:oatom * 3 + 3] - ccoords[matom * 3:matom * 3 + 3]  # b1
        wvec = ccoords[patom * 3:patom * 3 + 3] - ccoords[oatom * 3:oatom * 3 + 3]  # b2
        vvec = ccoords[natom * 3:natom * 3 + 3] - ccoords[patom * 3:patom * 3 + 3]  # b3
        ulen = linalg.norm(uvec)
        wlen = linalg.norm(wvec)
        vlen = linalg.norm(vvec)
        # check norms of the vectors
        if not _norm_is_ok(uvec, wvec, vvec):
            raise ValueError('A norm for dihedral {0} is either too large or small: ulen = {1}, wlen = {2}, vlen = {3}' \
                             .format(repr((matom, oatom, patom, natom)), ulen, wlen, vlen))
        ucross = np.cross(uvec / ulen, wvec / wlen)  # n1
        vcross = np.cross(wvec / wlen, vvec / vlen)  # n2
        phiu_sin = linalg.norm(ucross)
        phiv_sin = linalg.norm(vcross)
        dihed_rad = np.dot(ucross, vcross) / (phiu_sin * phiv_sin)
        if dihed_rad >= 1.0 - 1.0e-10:
            dihed_rad = 0.0
        elif dihed_rad <= -1.0 + 1.0e-10:
            dihed_rad = np.pi
        else:
            dihed_rad = np.arccos(dihed_rad)
        # adjust the sign
        if np.dot(uvec / ulen, vcross) < 0: dihed_rad = -dihed_rad
        # # always fix if state != 0
        if not nophase:
            if self.phase == 1 and dihed_rad < -np.pi/2.0:
                dihed_rad += 2.0*np.pi
            elif self.phase == -1 and dihed_rad > np.pi/2.0:
                dihed_rad -= 2.0*np.pi
        return dihed_rad

    def get_Deriv1(self, ccoords=None):
        assert ccoords is not None
        result = np.zeros(ccoords.shape)
        matom, oatom, patom, natom = self.atoms
        uvec = ccoords[matom * 3:matom * 3 + 3] - ccoords[oatom * 3:oatom * 3 + 3]
        wvec = ccoords[patom * 3:patom * 3 + 3] - ccoords[oatom * 3:oatom * 3 + 3]
        vvec = ccoords[natom * 3:natom * 3 + 3] - ccoords[patom * 3:patom * 3 + 3]
        ulen = linalg.norm(uvec)
        vlen = linalg.norm(vvec)
        wlen = linalg.norm(wvec)
        uvec /= ulen
        wvec /= wlen
        vvec /= vlen
        uwcross = np.cross(uvec, wvec)
        vwcross = np.cross(vvec, wvec)
        cos_u = np.dot(uvec, wvec)
        cos_v = -np.dot(vvec, wvec)
        sin2_u = 1.0 - cos_u ** 2
        sin2_v = 1.0 - cos_v ** 2
        if sin2_u <= 1.0e-12 or sin2_v <= 1.0e-12:  # torsion would be 0° or 180°
            warnings.warn('    Found torsion with linear angle for atoms {0},{1},{2},{3}.'.format(*self.atoms)\
                    +'Not calculating derivative!')
        else:
            result[matom * 3] = uwcross[0] / (ulen * sin2_u)
            result[matom * 3 + 1] = uwcross[1] / (ulen * sin2_u)
            result[matom * 3 + 2] = uwcross[2] / (ulen * sin2_u)
            result[oatom * 3] = -uwcross[0] / (ulen * sin2_u) + uwcross[0] * cos_u / (sin2_u * wlen) + \
                                              vwcross[0] * cos_v / (sin2_v * wlen)
            result[oatom * 3 + 1] = -uwcross[1] / (ulen * sin2_u) + uwcross[1] * cos_u / (sin2_u * wlen) + \
                                                  vwcross[1] * cos_v / (sin2_v * wlen)
            result[oatom * 3 + 2] = -uwcross[2] / (ulen * sin2_u) + uwcross[2] * cos_u / (sin2_u * wlen) + \
                                                  vwcross[2] * cos_v / (sin2_v * wlen)
            result[patom * 3] = vwcross[0] / (vlen * sin2_v) - uwcross[0] * cos_u / (sin2_u * wlen) - \
                                              vwcross[0] * cos_v / (sin2_v * wlen)
            result[patom * 3 + 1] = vwcross[1] / (vlen * sin2_v) - uwcross[1] * cos_u / (sin2_u * wlen) - \
                                                  vwcross[1] * cos_v / (sin2_v * wlen)
            result[patom * 3 + 2] = vwcross[2] / (vlen * sin2_v) - uwcross[2] * cos_u / (sin2_u * wlen) - \
                                                  vwcross[2] * cos_v / (sin2_v * wlen)
            result[natom * 3] = -vwcross[0] / (vlen * sin2_v)
            result[natom * 3 + 1] = -vwcross[1] / (vlen * sin2_v)
            result[natom * 3 + 2] = -vwcross[2] / (vlen * sin2_v)
        return result

    def get_Deriv2(self, ccords=None):
        return NotImplemented


pics_by_atoms = { 2: (Stretch,), 3: (Bend,), 4: (Twist,)}
