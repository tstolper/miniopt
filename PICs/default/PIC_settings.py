
pic_constants = { 'covbond_fact': 1.3, 'hbond_fact': 0.9, 'auxinter_ang': 2.0, 'auxinter_fact': 1.3, 'auxbond_fact': 2.5}
pic_max_atoms = 4
acceptor_elements   = ('N', 'O', 'F', 'P', 'S', 'Cl')
donor_elements      = acceptor_elements
bond_types = {'standard': 1, 'hbond': 2, 'extra': 4}
