
# --* coding: utf-8 *--

from . import PIC_settings
from . import PIC_classes

def return_PIC_Classes(): pass

def _get_n_Atom_Combinations(natoms=None, ncombin=None, start=0):
    assert None not in (natoms, ncombin)
    atom_ids = []
    for i in range(start, natoms-ncombin+1):
        if ncombin > 1:
            atom_ids.extend([(i,) + idxs for idxs in _get_n_Atom_Combinations(natoms, ncombin-1, i+1)])
        else:
            atom_ids.append((i,))
    return atom_ids

def get_PICs(ccoords=None, distances=None, *args, **kwargs):
    assert ccoords is not None
    assert distances is not None

    natoms  = len(ccoords) // 3
    max_atm = PIC_settings.pic_max_atoms
    bonds   = PIC_classes.get_bonds(ccoords, args[0], distances)

    for atms_per_pic in range(2, max_atm+1):
        for pic in PIC_classes.get_PICs_by_atomcomb(_get_n_Atom_Combinations(natoms, atms_per_pic), distances, bonds, ccoords, symbols=args[0]):
            yield pic
