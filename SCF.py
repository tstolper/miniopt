
# --* coding: utf-8 *--

from .Interface.Molpro import MolproRun

class Calculation(object):
    
    def __init__(self, prog=None, procs=None):
        self.program        = prog
        self.nprocs         = procs
        self.curr_Energy    = None
        self.curr_Gradient  = None
        self.curr_Hessian   = None
        self.curr_coords    = None


class MolproSCF(Calculation):
    calc_template = """memory,200,m
geomtyp=xyz
geometry={structure}
basis=tzvp
df-ks,pbe,grid=1.0d-8
forces"""
    en_match = """!RKS STATE 1.1 Energy"""

    def __init__(self, **kwargs):
        tmpfile_name = kwargs.pop('tempname', 'tmpSCF')
        input_template = kwargs.pop('template', self.calc_template)
        exe_path = kwargs.pop('program', '/opt/software/molpro_2012.1.p23/bin_mpp2012.p23/molprop')
        run_options = kwargs.pop('options', '--no-xml-output -d /scratch/kumo -n 4')
        processors = kwargs.pop('nprocs', 4)
        self.en_match = kwargs.pop('en_match', self.en_match)
        qmprog = MolproRun(basepath=tmpfile_name, template=input_template, prog=exe_path, options=run_options)
        super(MolproSCF, self).__init__(prog=qmprog, procs=processors)

    def get_EnGrad(self, mol=None):
        self.program.run_calc(mol.get_String(noheader=True))
        energy = self.program.get_Energy(match_str=self.en_match)
        gradient = self.program.get_Gradient()
        return energy, gradient.flatten()


class OrcaSCF(Calculation):
    calc_template = """ """
    en_match = """ """

    def __init__(self):
        pass
