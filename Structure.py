
# --* coding: utf-8 *--

import numpy as np

from miniopt.utilities import Elements, Bohr2Ang


class _MolStates(object):

    def __init__(self, *args):
        self.num_atoms = args[0]
        self.comment = args[1]
        self.coords = args[2]

        if len(args) > 3:
            self.extra = args[3]
        else:
            self.extra = None


class Molecule(object):

    def __init__(self, from_mol=None, ccoords=None):
        if from_mol is None:
            self.states = []
            self.symbols = None
            self.ids = None
            self.cartesian = None
        else:
            assert ccoords is not None
            other = from_mol.states[0]
            self.states = [_MolStates(other.num_atoms, other.comment, ccoords,
                                      other.extra)]
            self.symbols = from_mol.symbols
            self.ids = from_mol.ids
            self.cartesian = ccoords

    def _read_xyz_once(self, stream=None):
        assert stream is not None
        try:
            natoms = int(next(stream))
            comment = next(stream)
            symbols = []
            ids = []
            coords = np.zeros((natoms*3,))
            extinfo = None
            catom = 0
            for line in stream:
                __ = line.split()
                if len(__) > 3:
                    ids.append(__[0])
                    symbols.append(__[0].rstrip('0123456789').capitalize())
                    # check that we can handle the atom type!
                    message = 'Element {0:5} not in list: {1}'.format(
                        symbols[-1], Elements._fields)
                    assert symbols[-1] in Elements._fields, message
                    coords[catom * 3] = float(__[1]) / Bohr2Ang
                    coords[catom * 3 + 1] = float(__[2]) / Bohr2Ang
                    coords[catom * 3 + 2] = float(__[3]) / Bohr2Ang
                    if len(__) > 4:
                        if extinfo is None:
                            extinfo = []
                        extinfo.append(' '.join(__[3:]))
                    catom += 1
            if catom != natoms:
                raise Exception('Number of atoms read does not correspond '
                                'to signalled number!')
            return _MolStates(natoms, comment, coords, extinfo), symbols, ids
        except ValueError as e:
            raise ValueError('String cannot be converted to number!')

    def read_from_file(self, filename=None):
        """ Reads molecular structures from a file.
        The file type is determined by the extension of the file.
        """
        assert filename is not None
        ext = filename.split('.')[-1].lower()

        if ext == 'xyz':
            with open(filename, 'r') as ifile:
                state, symbols, ids = self._read_xyz_once(ifile)
                self.states.append(state)
                if self.symbols is None:
                    self.symbols = symbols
                if self.ids is None:
                    self.ids = ids
                if self.cartesian is None:
                    self.cartesian = self.states[0].coords
        elif ext == 'trj':
            pass
        else:
            raise ValueError('File extension {0} not yet handled!'.format(ext))

    def read_from_string(self, string=None, ext='xyz'):
        assert string is not None
        if ext == 'xyz':
            state, symbols, ids = self._read_xyz_once(
                iter(string.splitlines()))
            self.states.append(state)
        if self.symbols is None:
            self.symbols = symbols
        if self.ids is None:
            self.ids = ids
        if self.cartesian is None:
            self.cartesian = self.states[0].coords
        else:
            raise ValueError('File extension {0} not yet handled!'.format(ext))

    def get_String(self, noheader=False):
        if not noheader:
            result = '{:5d}\n{:s}'.format(
                len(self.symbols), self.states[0].comment)
        else:
            result = ''
        for elem, xyz in zip(self.symbols,
                             self.cartesian.reshape(len(self.symbols), 3)):
            result += '\n{:s} {:12.8f} {:12.8f} {:12.8f}'.format(
                elem, *(Bohr2Ang*xyz))
        return result

    def get_state_coords(self, state=0):
        return self.states[state].coords

    def get_cart_Displacements(self, dspl_type='Grad2NumHess', displ=0.01):
        result = Molecule(from_mol=self, ccoords=self.cartesian)
        position = ''.join()
        if dspl_type == 'Grad2NumHess':
            for i in range(len(self.cartesian)):
                for j in range(i, len(self.cartesian)):
                    pass
                    # result.states.append(_MolStates(result.num_atoms, str(i),
                    #                                 self.cartesian + np.))
