#! /usr/bin/env python3
from numpy import sqrt, mean, power, max
from numpy.linalg import norm
from miniopt.Structure import Molecule
from miniopt.CoordSets import Redundant
from miniopt.Hessians import Diagonal
from miniopt.Minimizers import BFGS, get_RFO_Step, MolproConv
from miniopt.SCF import MolproSCF
from miniopt.tests import histidine

# 1. Load the molecule
mol = Molecule()
mol.read_from_string(histidine)
# 2. Create primitive Internals
coords = Redundant(mol)
# 3. Choose a guess Hessian
hess = Diagonal(start_coords=coords)
# 4. And a level of theory
singlepoint = MolproSCF()
# 5. Assemble into a minimization
opt = BFGS(step_func=get_RFO_Step, coordset=coords, hessian=hess, scf=singlepoint, convcrit=MolproConv)
# 6. And iterate through the steps
step = 1
old_cart = mol.cartesian
old_en = opt.optstuff[-1][1]
old_grad = opt.optstuff[-1][2]
for curr_en, curr_grad, curr_cart, curr_int in opt:
    dx = curr_cart - old_cart
    print('Step {:3d} RMSD(cart): {:12.2e} StepLen(cart): {:12.2e} dE(Eh): {:12.2e} MaxGrad-1(cart): {:12.2e}'.format(
            step, sqrt(mean(power(dx, 2))), norm(dx), curr_en-old_en, max(old_grad)))
    step += 1
    old_cart = curr_cart
    old_en = curr_en
    old_grad = curr_grad
