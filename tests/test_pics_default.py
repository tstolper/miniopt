#!/usr/bin/env python3
# --* coding: utf-8 *--

import numpy as np
from miniopt.Structure import Molecule
from miniopt.PICs import PICList
from miniopt.tests import histidine

# results from PyOpt
ref_coordinates = [(0,1), (0,2), (0,11), (1,3), (1,5), (2,4), (3,4), (3,12), (4,13), (5,6), (5,14), (5,15), (6,7),
                   (6,8), (6,16), (7,9), (7,10), (8,17), (8,18), (10,19), (0,1,3), (0,1,5), (0,2,4), (1,0,2), (1,0,11),
                   (1,3,4), (1,3,12), (1,5,6), (1,5,14), (1,5,15), (2,0,11), (2,4,3), (2,4,13), (3,1,5), (3,4,13),
                   (4,3,12), (5,6,7), (5,6,8), (5,6,16), (6,5,14), (6,5,15), (6,7,9), (6,7,10), (6,8,17), (6,8,18),
                   (7,6,8), (7,6,16), (7,10,19), (8,6,16), (9,7,10), (14,5,15), (17,8,18), (0,1,3,4), (0,1,3,12),
                   (0,1,5,6), (0,1,5,14), (0,1,5,15), (0,2,4,3), (0,2,4,13), (1,0,2,4), (1,3,4,2), (1,3,4,13),
                   (1,5,6,7), (1,5,6,8), (1,5,6,16), (2,0,1,3), (2,0,1,5), (2,4,3,12), (3,1,0,11), (3,1,5,6),
                   (3,1,5,14), (3,1,5,15), (4,2,0,11), (4,3,1,5), (5,1,0,11), (5,1,3,12), (5,6,7,9), (5,6,7,10),
                   (5,6,8,17), (5,6,8,18), (6,7,10,19), (7,6,5,14), (7,6,5,15), (7,6,8,17), (7,6,8,18), (8,6,5,14),
                   (8,6,5,15), (8,6,7,9), (8,6,7,10), (9,7,6,16), (9,7,10,19), (10,7,6,16), (12,3,4,13), (14,5,6,16),
                   (15,5,6,16), (16,6,8,17), (16,6,8,18)]
ref_values = np.array([2.654319, 2.624583, 2.040795, 2.604645, 2.884047, 2.591608, 2.591636, 1.936978, 2.046725, 2.936020,
              2.090320, 2.092004, 2.937046, 2.810156, 2.093081, 2.323957, 2.578561, 1.940182, 1.940129, 1.811309,
              1.917874, 2.205614, 1.725536, 1.940315, 2.171637, 1.747124, 2.265859, 2.016565, 1.876412, 1.917343,
              2.171222, 2.093923, 2.083869, 2.159649, 2.105374, 2.269376, 1.975287, 1.919742, 1.888826, 1.909504,
              1.918787, 2.182072, 1.963841, 1.930809, 1.923320, 1.927771, 1.858246, 1.877235, 1.890813, 2.137200,
              1.813802, 1.841966, 0.000164, -3.100520, -1.648957, 2.496664, 0.533698, -0.003368, 3.131456, 0.003120,
              0.002140, -3.132600, -2.868829, 1.248875, -0.813713, -0.002245, -3.133604, 3.102703, -3.139039, 1.504208,
              -0.633357, -2.596322, 3.139915, 3.131848, 0.012787, 0.031164, -0.567711, 2.561174, -3.057971, -1.027740,
              3.117999, -0.748966, 1.232446, 1.032420, 3.062652, -2.914448, -0.933036, 1.593377, -1.560923, -2.640598,
              -0.035926, 0.488287, -0.032037, 1.306150, -2.995623, -0.996610, 1.033621])
ref_ncoords = 97

assert len(ref_coordinates) == ref_ncoords
assert len(ref_values) == ref_ncoords

mol = Molecule()
mol.read_from_string(histidine)
coords = PICList(mol=mol)
coords.determine_PICs()
coord_tuples = coords.get_PIC_Tuples()
coord_vals = coords.get_Values()
assert len(coord_tuples) == ref_ncoords, 'len(coord_tuples)='+str(len(coord_tuples))+', ref_ncoords='+str(ref_ncoords)
assert len(coord_vals) == ref_ncoords
coord_comp = [pic in ref_coordinates for pic in coord_tuples]
assert False not in coord_comp, 'Not the same coordinates (tuples)!'
assert coord_tuples == ref_coordinates, 'Not the same order of coordinates!' + repr([coord_tuples[i] == ref_coordinates[i] for i in range(ref_ncoords)])
assert np.allclose(coord_vals, ref_values, atol=1.e-6), 'coord_vals-ref_values: ' + str(coord_vals-ref_values)
# TODO: Add check of the 2nd derivatives
diff1_analytic = coords.get_Deriv1()
diff1_numeric = coords.get_NumDeriv1()
if np.allclose(diff1_analytic, diff1_numeric, atol=1.e-6):
    print('Derviatives ok.')
else:
    'Derivatives not accurate! maximum deviation per pic:\n'+'\n'.join(
    '{:5d} {:8.6f}'.format(i, np.max(v)) for i,v in zip(range(diff1_analytic.shape[0]), diff1_analytic-diff1_numeric))
