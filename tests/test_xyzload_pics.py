#! /usr/bin/env python3
# --* coding: utf-8 *--

from miniopt.Structure import Molecule
from miniopt.PICs import PICList

mol = Molecule()
mol.read_from_file('histidine.xyz')
coords = PICList(mol=mol)
coords.determine_PICs()
for c,v in zip(str(coords), coords.get_Vals()):
    print '{:20}     value={:8.5f}'.format(c, v)