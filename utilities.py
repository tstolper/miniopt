
# -*- coding: utf-8 -*-
# E.g. atomic masses are from NIST.
# Covalent radii are from: Pyykkö, Atsumi, Chem. Eur. J. 2009, 15, 186--197.
# vdW radii are from: Alvarez, Dalton Trans., 2013, 42, 8617.
# Bohr radius is also from NIST: http://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0|search_for=atomnuc!

from collections import namedtuple
import scipy as np
import scipy.linalg as linalg
from scipy import arccos, dot, pi
from scipy.linalg import norm

Bohr2Ang = 0.52917721092
Eh2cm = 219474.6313705
Eh2kJmol = 2625.49962
Eh2J = 4.359744e-18
const_c = 299792458.0
EhBohr22Jm2 = 1556.8928448170257
amu2kg = 1.660538782E-27
ElementList     = namedtuple('ElementList', 'H He Li Be B C N O F Ne Na Mg Al Si P S Cl Ar Fe Ni Cu Ru')
ElementProp     = namedtuple('ElementProp', 'number mass cov_radius vdw_radius')
Elements        = ElementList(H = ElementProp(1, 1.00794,   32E-2/Bohr2Ang, 120E-2/Bohr2Ang),
                            He = ElementProp(2, 4.002602,   46E-2/Bohr2Ang, 143E-2/Bohr2Ang),
                            Li = ElementProp(3, 6.941,     133E-2/Bohr2Ang, 212E-2/Bohr2Ang),
                            Be = ElementProp(4, 9.012182,  102E-2/Bohr2Ang, 198E-2/Bohr2Ang),
                            B  = ElementProp(5, 10.811,     85E-2/Bohr2Ang, 191E-2/Bohr2Ang),
                            C  = ElementProp(6, 12.0107,    75E-2/Bohr2Ang, 177E-2/Bohr2Ang),
                            N  = ElementProp(7, 14.0067,    71E-2/Bohr2Ang, 166E-2/Bohr2Ang),
                            O  = ElementProp(8, 15.9994,    63E-2/Bohr2Ang, 150E-2/Bohr2Ang),
                            F  = ElementProp(9, 18.9984032, 64E-2/Bohr2Ang, 146E-2/Bohr2Ang),
                            Ne = ElementProp(10,20.1797,    67E-2/Bohr2Ang, 158E-2/Bohr2Ang),
                            Na = ElementProp(11,22.98976928,155E-2/Bohr2Ang, 250E-2/Bohr2Ang),
                            Mg = ElementProp(12,24.3050,    139E-2/Bohr2Ang, 251E-2/Bohr2Ang),
                            Al = ElementProp(13,26.9815386, 126E-2/Bohr2Ang, 225E-2/Bohr2Ang),
                            Si = ElementProp(14,28.0855,    116E-2/Bohr2Ang, 219E-2/Bohr2Ang),
                            P  = ElementProp(15,30.973762,  111E-2/Bohr2Ang, 190E-2/Bohr2Ang),
                            S  = ElementProp(16,32.065,     103E-2/Bohr2Ang, 189E-2/Bohr2Ang),
                            Cl = ElementProp(17,35.453,      99E-2/Bohr2Ang, 182E-2/Bohr2Ang),
                            Ar = ElementProp(18,39.948,      96E-2/Bohr2Ang, 183E-2/Bohr2Ang),
                            Fe = ElementProp(26,55.845,     116E-2/Bohr2Ang, 244E-2/Bohr2Ang),
                            Ni = ElementProp(28,58.6934,    110E-2/Bohr2Ang, 240E-2/Bohr2Ang),
                            Cu = ElementProp(29,63.546,     112E-2/Bohr2Ang, 238E-2/Bohr2Ang),
                            Ru = ElementProp(44,101.07,     125E-2/Bohr2Ang, 246E-2/Bohr2Ang))


def get_Period(elem=None):
    assert elem != None

    retval = 0
    massnum = getattr(Elements, elem).number
    if massnum <= 2:
        retval = 1
    elif massnum <= 10:
        retval = 2
    elif massnum <= 18:
        retval = 3
    elif massnum <= 36:
        retval = 4
    return retval

def dij(a = None, b = None):
    assert a != None
    assert b != None
    retval = 0
    if a == b: retval = 1
    return retval

def zijk(i = None, j = None, k = None):
    assert i != None
    assert j != None
    assert k != None
    retval = 0
    if i == j:
        retval = 1
    elif i == k:
        retval = -1
    return retval

def symm_mat_inv(mat = None, redundant = False):
    det = 1.0
    evals, evecs = linalg.eigh(mat)
    log_Vector('    Eigenvalues of to be inverted matrix:', evals)

    for val in evals:
      det *= val

    if not redundant and np.absolute(det) < 1.0e-10:
      raise ValueError('Couldn\'t inverse non-generalized matrix!')

    tmpmat = np.zeros(mat.shape)
    for i, e in enumerate(evals):
      if not redundant or np.absolute(e) > 1.0e-10:
        tmpmat[i,i] = 1.0/e
    log_Vector('    Eigenvalues of inverted matrix:', evals)

    retmat = np.dot(evecs, np.dot(tmpmat, evecs.transpose()))
    return retmat

def _are_parallel(uvec = None, vvec = None, thresh = 1.0e-10):
    assert uvec != None
    assert vvec != None
    retval  = True
    a= uvec/np.linalg.norm(uvec)
    b= vvec/np.linalg.norm(vvec)
    if np.absolute(np.absolute(np.dot(a,b)) - 1.0) > thresh:
        logverbose('    Vectors not parallel. a.b={:12.8f}'.format(np.dot(a,b)))
        retval = False
    return retval

def _norm_is_ok(*args, **kwargs):
    if 'min' in kwargs:
        normmin = kwargs['min']
    else:
        normmin = 1.0e-8
    if 'max' in kwargs:
        normmax = kwargs['max']
    else:
        normmax = 1.0e+15
    retval = True
    for vec in args:
        val = norm(vec)
        if val < normmin or val > normmax:
            retval = False
            break
    return retval

def _calcDist(flatcoords=None, fatom=None, iatom=None):
    return norm(flatcoords[fatom*3:fatom*3+3] - flatcoords[iatom*3:iatom*3+3])

def _calcAngle(flatcoords = None, latom = None, catom = None, ratom = None):
    lvec        = flatcoords[latom*3:latom*3+3] - flatcoords[catom*3:catom*3+3]
    rvec        = flatcoords[ratom*3:ratom*3+3] - flatcoords[catom*3:catom*3+3]
    return _calc_Angle_of_Vects(lvec, rvec)[0]

def _calc_Angle_of_Vects(vec1 = None, vec2 = None):
    assert vec1 is not None
    assert vec2 is not None
    angle_rad = None
    v1len   = norm(vec1)
    v2len   = norm(vec2)
    if not _norm_is_ok(vec1, vec2):
        raise ValueError('A norm for an angle is either too large or small: v1len = {0}, v2len = {1}'.format(v1len, v2len))
    else:
        angle_cos = dot(vec1/v1len, vec2/v2len)
        if angle_cos >= 1.0 - 1.0e-14:
            angle_rad = 0.0
        elif angle_cos <= -1.0 + 1.0e-14:
            angle_rad = pi
        else:
            angle_rad = arccos(angle_cos)
    return angle_rad, angle_cos

def _calcDihedral(flatcoords = None, matom = None, oatom = None, patom = None, natom = None):
    """
    See: http://math.stackexchange.com/questions/47059/how-do-i-calculate-a-dihedral-angle-given-cartesian-coordinates
    """
    uvec    = flatcoords[oatom*3:oatom*3+3] - flatcoords[matom*3:matom*3+3] # b1
    wvec    = flatcoords[patom*3:patom*3+3] - flatcoords[oatom*3:oatom*3+3] # b2
    vvec    = flatcoords[natom*3:natom*3+3] - flatcoords[patom*3:patom*3+3] # b3
    ulen    = linalg.norm(uvec)
    wlen    = linalg.norm(wvec)
    vlen    = linalg.norm(vvec)
    if not _norm_is_ok(uvec, wvec, vvec):
        raise ValueError('A norm for dihedral {0} is either too large or small: ulen = {1}, wlen = {2}, vlen = {3}'\
                         .format(repr((matom, oatom, patom, natom)), ulen, wlen, vlen))
    ucross  = np.cross(uvec/ulen, wvec/wlen) # n1
    vcross  = np.cross(wvec/wlen, vvec/vlen) # n2
    phiu_sin = linalg.norm(ucross)
    phiv_sin = linalg.norm(vcross)
    # sensibility check if not done beforehand
    # if phiu_sin * phiv_sin < 2.0e-3:
    #     print 'dihedral:', repr((matom, oatom, patom, natom))
    #     print 'phiu_sin:', repr(phiu_sin)
    #     print 'phiv_sin:', repr(phiv_sin)
    #     raise ValueError('At least one angle in the dihedral is ~0. Stopping the calculation.')
    # adjust the sign
    n1          = ucross/linalg.norm(ucross)
    n2          = vcross/linalg.norm(vcross)
    mvec        = np.cross(n1, wvec/wlen)
    tors_rad    = -np.arctan2(np.dot(mvec, n2), np.dot(n1, n2))
    return ((matom, oatom, patom, natom), None, tors_rad), (uvec, wvec, vvec, ulen, wlen, vlen, phiu_sin, phiv_sin)

def _get_Set_of_nonlin_BP(flatcoords=None, catom=None, oatom=None, bonds=None):
    ret_bps = []
    bonding_atoms = np.nonzero(bonds[catom, :])
    for atom in bonding_atoms:
        if atom != oatom:
            angle = _calcAngle(flatcoords, atom, catom, oatom)[0][2]
            if angle < np.pi:
                ret_bps.append(atom)
    return ret_bps
